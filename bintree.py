# Test task for Brenmann17, Dmytro Povesma, Jan 2021
# Serializing / deserializing binary tree
# Usage:
# import Node from bintree.py
# # create a tree
# t = Node.create()
import json, math, random

class Node:
	value = None
	child1 = None
	child2 = None

	def add(self, value):
		if self.value is None:
			self.value = value
		else:
			if value >= self.value:
				if self.child1 is None:
					self.child1 = Node()
				self.child1.add(value)
			else:
				if self.child2 is None:
					self.child2 = Node()
				self.child2.add(value)
	def toJson(self):
		j = {"value":self.value, "child1": self.child1.toJson() if self.child1 else None,
				"child2": self.child2.toJson() if self.child2 else None}
		return j

	def toJsonString(self):
		return json.dumps(self.toJson())

	def fromJson(j):
		n = Node()
		if type(j) is not dict:
			# try to parse a list from string
			try:
				j = json.loads(j)
			except Exception as e:
				print ("Wrong JSON format:", e)
				return
		n.value = j.get("value")
		n.child1 = Node.fromJson(j.get("child1")) if j.get("child1") else None
		n.child2 = Node.fromJson(j.get("child2")) if j.get("child2") else None
		return n

	def serialize_str(self): # outputs a string
		return json.dumps(self.serialize())

	def serialize(self): # outputs an array
		res = []
		if self.value is not None:
			res = res + [self.value,
							self.child1.value is not None if self.child1 else None,
							self.child2.value is not None if self.child2 else None]
			if self.child1 and self.child1.value is not None:
				res = res + self.child1.serialize()
			if self.child2 and self.child2.value is not None:
				res = res + self.child2.serialize()
			return res
		else:
			return []

	
	def print(root, space = 0) : 
		COUNT = 7
		# Base case  
		if (root == None) : 
			return
	  
		# Increase distance between levels  
		space += COUNT
	  
		# Process right child first  
		Node.print(root.child2, space)  
	  
		# Print current node after space  
		# count  
		print()  
		for i in range(COUNT, space): 
			print(end = " ")  
		print(root.value)  
	  
		# Process left child  
		Node.print(root.child1, space)  
	def sample():
		top_n = Node()
		top_n.value = 1

		l_1 = Node()
		l_1.value = 2

		r_1 = Node()
		r_1.value = 3

		r_2 = Node()
		r_2.value = 4

		r_3 = Node()
		r_3.value = 5

		r_1.child1 = r_2
		r_1.child2 = r_3
		top_n.child1 = l_1
		top_n.child2 = r_1
		return top_n
#s = top_n.serialize()
#print(s)

#a = [4,2,6,2,3,1,6,2,3,4,7,6,5,3,4,8,9,1,2,8,3]
	def create(a, n): #values range -a; a, n nodes
		N = Node()
		for x in range(n):
			x = math.floor(random.random()*2*a)-a
			N.add(x)
		return N
#Node.print(n,0)
# [1, 2, 3, 2, None, None, 3, 4, 5, 4, None, None, 5, None, None]
	def deserialize_tree(arr):
		t, a = Node.deserialize_node(arr)
		return t

	def deserialize_node(arr):
		if type(arr) is not list:
			# try to parse a list from string
			try:
				arr = json.loads(arr)
			except Exception as e:
				print ("Wrong data format:", e)
				return
		node = Node()
		node.value = arr[0]
		child1_value = arr[1]
		child2_value = arr[2]
		new_arr = arr
		if child1_value is not None:
			new_arr = new_arr[3:]
			(node.child1, new_arr) = Node.deserialize_node(new_arr)
		if child2_value is not None:
			new_arr = new_arr[3:]
			(node.child2, new_arr) = Node.deserialize_node(new_arr)
		return (node, new_arr)


# node = {[id, value, [{[id, value, [node1, node1]]}, {[id, value, [node1, node1]]}]]}
# 1,2,3,null,null,3,4,5  = serialized format
