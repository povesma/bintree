
import unittest
from bintree import Node

class TestBinaryTree(unittest.TestCase):

	def test_sample(self):
		sample_tree = Node.sample()
		s = sample_tree.serialize_str()
		d = Node.deserialize_tree(s).serialize_str()
		self.assertEqual(s, d)

	def test_single_random(self):
		tree = Node.create(1000, 104) #max ranges per specs
		s = tree.serialize_str()
		d = Node.deserialize_tree(s).serialize_str()
		self.assertEqual(s, d)

	def test_multi_random(self): # run 100 tests
		for i in range(100):
			tree = Node.create(1000, 104) #max ranges per specs
			s = tree.serialize_str()
			d = Node.deserialize_tree(s).serialize_str()
			self.assertEqual(s, d)

	def test_single_random_json(self):
		tree = Node.create(1000, 104) # max ranges per specs
		s = tree.toJsonString()
		d = Node.fromJson(s).toJsonString()
		self.assertEqual(s, d)

	def test_multi_random_json(self):
		for i in range(100):
			tree = Node.create(1000, 104) # max ranges per specs
			s = tree.toJsonString()
			d = Node.fromJson(s).toJsonString()
			self.assertEqual(s, d)

if __name__ == '__main__':
    unittest.main()