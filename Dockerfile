FROM jenkins/jenkins:lts
USER root
RUN apt-get update && \
apt-get -y install apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update
RUN apt-get -y install docker-ce nodejs
RUN apt -y  install gcc g++ make
#RUN apt-get -y install python3
#RUN apt-get -y install python-pip
#RUN pip install mkdocs
#RUN pip install mkdocs-material

RUN usermod -a -G docker jenkins
USER jenkins