#  Binary tree serialization / deserialization [Python]
## Specs:

Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. 

There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.

number of nodes: [0, 104]

Values of the nodes -1000 <= val <= 1000

## Use
```
from bintree import Node
# create a tree,

# either a sample tree:
t = Node.sample()

# or random:
t = Node.create(1000, 104) # 1000 means range of random values-1000; 1000, 104 - number of nodes

# or adding the nodes manually:
t = Node()
t.add(1) # adds the root node
t.add(2) # adds the node to the branch comparing the value wuth the root value
t.add(3)
t.add(4)
t.add(5)

# print it to see:
t.print()

#serialize to a simple string
s = t.serialize_str()

# or to json string
j = t.toJsonString()

# deserialize from string
new_tree = Node.deserialize_tree(s)

# or deserialize from json string
new_tree = Node.fromJson(j)

```

## Tests
Tests are based on comparing serialized string with serilizes-deserialized-serializes result

Run the tests with:

`python3 -m unittest`

# Elasticsearch + CI

## Script

`cd scripts; npm install node es.js`

### CLI params:

`node es.js load [URL] | index index_name | search hero_name`

Elasticsearch runs on http://k8.cloudi.es:9200 , unprotected

## CI - Jenkins

Custom Jenkins images built with `Dockerfile` - added docker CLI + NodeJS to the image.

Available at http://k8.cloudi.es:9876 (ask for credentials).

Jenkins job: "Execute shell" - 
```
cd /scripts

npm install
env ES_URL=http://elasticsearch:9200 node es.js index index_$BUILD_ID
```

## Deployment

1. Build Docker image for Jenkins: `docker build -t jendoc .`
1. Run `docker-compose up -d`
1. Create a job in `Jenkins`
1. Done

### Usage

1. Use `es.js` to create an index `hero` (`node es.js index hero`)
1. Use `es.js` to add CSV to the index `hero` (`node es.js load`)
1. Use `es.js` to search (`node es.js search HERO NAME`)
1. Run Jenkins job `CreateIndex` to create an index `index_$BUILD_ID`


