const fs = require("fs");
const https = require('https');
const csvParser = require('csv-parse/lib/sync');
var CSV_URL = "https://raw.githubusercontent.com/royrusso/elasticsearch-sample-index/master/indexme.csv";
const esUrl = process.env["ES_URL"] || 'http://localhost:9200';
console.log(process.argv);
var ACTION = null;
var indexName = null;
var searchName = null;
var param = null;
if (process.argv[2]) {
	if (process.argv[2] == 'load') {
		ACTION = "load";
		CSV_URL = process.argv[3] || CSV_URL;
		param = CSV_URL;
	} 

	if (process.argv[2] == 'list') {
		ACTION = "list";
	} 

	if (process.argv[2] == 'index') {
		if (process.argv[3]) {
			ACTION = "createIndex";
			indexName = process.argv[3]
			param = indexName;
		} else {
			usage();
		}
	} 
	if (process.argv[2] == 'search') {
		if (process.argv[3]) {
			ACTION = "search";
			searchName = process.argv.slice(3).join(' ')
			param = searchName;
		} else {
			usage();
		}
	}
	if (!ACTION) {
		usage();
	}
} else {
	usage();
}

function usage() {
	console.log("Usage: node es.js load [URL] | index index_name | search hero_name");
	process.exit(0);
}

console.log("Running with: ACTION:", ACTION,", param:", param);

const { Client } = require('@elastic/elasticsearch')
const client = new Client({
  node: esUrl
});

client.cluster.remoteInfo().
	then(d => {
		console.log("Connected to ES");
		if (ACTION == "load") {
			loadData(CSV_URL);
		}
		if (ACTION == "createIndex") {
			createIndex(indexName);
		}
		if (ACTION == "search") {
			search(searchName);
		}
		if (ACTION == "list") {
			listIndices();
		}
	}).
	catch(e => console.log("Not connected...", e));


// bulk add
function loadData(url) {
	loadURL(url, csv => {
		arr = csvParser(csv);
		dataset = arr.map(e => { return {hero: e[0], description: e[1]}});
		const body = dataset.flatMap(doc => [{ index: { _index: 'hero' } }, doc])
		bulk(body);
	});
}

function createIndex(indexName) {
	client.indices.create({
	    index: indexName,
	    body: {
	      mappings: {
	        properties: {
	          hero: { type: 'text' },
	          description: { type: 'text' }
	        }
	      }
	    }
	  }, { ignore: [400] }).
		then(d => {
			console.log("Index created");
			// check index: 
			client.indices.stats().then(d => {
				console.log(d.body.indices[indexName].uuid);
				listIndices();
			});
		}).
		catch(e => console.log("Index creating failed:", e));
}

function listIndices() {
	client.cat.indices().
		then(d => console.log("All indicies:\n" + d.body)).
		catch(e => console.log("err getting indices:", e));
}
function bulk(body) {
	client.bulk({ refresh: true, body }).
		then(d => {console.log("OK", d);}).
		catch(e => {console.log("Error with bulk upload!", e);});
}

function search(hero) {
	console.log('Searching for "', hero, '"');
	client.search({
	  index: 'hero',
	  body: {
	    query: {
	      match: { hero: hero }
	    }
	  }
	}, (err, result) => {
	  if (err) {
	  	console.log('err:', err)
	  } else {
	  	console.log('data:', getSearchRes(result)); // , "\nTotal:", result.body.hits.total
	  }
	});
}

function getSearchRes(result) {
	return (result.body.hits && result.body.hits.hits && result.body.hits.hits.length) ? 
		result.body.hits.hits.map(e => e._source) : "Not found"
}


function loadURL(url, callback) {
	https.get(url, (resp) => {
	  let data = '';

	  // A chunk of data has been received.
	  resp.on('data', (chunk) => {
	    data += chunk;
	  });

	  // The whole response has been received. Print out the result.
	  resp.on('end', () => {
	    callback(data);
	  });

	}).on("error", (err) => {
	  console.log("Error loading URL: ", url, ": ", err.message);
	});
}
